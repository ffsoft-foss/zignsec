<?php

namespace ffsoft\zignsec;

use CURLFile;
use Doctrine\Common\Annotations\AnnotationRegistry;
use ffsoft\zignsec\enums\Methods;
use ffsoft\zignsec\exceptions\ClientException;
use ffsoft\zignsec\messages\requests\addDocsToScanningSession\AddDocsToScanningSessionRequest;
use ffsoft\zignsec\messages\requests\createScanningSession\CreateScanningSessionRequest;
use ffsoft\zignsec\messages\requests\getDocsAnalysisResults\GetDocsAnalysisResultsRequest;
use ffsoft\zignsec\messages\requests\getDocsAnalysisStatus\GetDocsAnalysisStatusRequest;
use ffsoft\zignsec\messages\requests\Request;
use ffsoft\zignsec\messages\requests\startDocsAnalysis\StartDocsAnalysisRequest;
use ffsoft\zignsec\messages\requests\watchlistGlobal\WatchlistGlobalRequest;
use ffsoft\zignsec\messages\requests\watchlistNordic\WatchlistNordicRequest;
use ffsoft\zignsec\messages\responses\addDocsToScanningSession\AddDocsToScanningSessionResponse;
use ffsoft\zignsec\messages\responses\BaseResponse;
use ffsoft\zignsec\messages\responses\createScanningSession\CreateScanningSessionResponse;
use ffsoft\zignsec\messages\responses\Error;
use ffsoft\zignsec\messages\responses\ErrorResponse;
use ffsoft\zignsec\messages\responses\getDocsAnalysisResults\GetDocsAnalysisResultsResponse;
use ffsoft\zignsec\messages\responses\getDocsAnalysisStatus\GetDocsAnalysisStatusResponse;
use ffsoft\zignsec\messages\responses\startDocsAnalysis\StartDocsAnalysisResponse;
use ffsoft\zignsec\messages\responses\watchlistGlobal\WatchlistGlobalResponse;
use ffsoft\zignsec\messages\responses\watchlistNordic\WatchlistNordicResponse;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use Throwable;

/**
 * Class Client implements ZignSec API methods
 *
 * @package ffsoft\zignsec
 */
class Client
{
    /**
     * This header parameter is the personal access token you received from ZignSec during the registration process.
     *
     * @example 12345678-e2a2-4968-b651-5352305c9fb0
     * @var string
     */
    protected $accessToken;
    /**
     * ZignSec server url
     *
     * @example https://test.zignsec.com
     * @var string
     */
    protected $baseUrl;
    /**
     * cURL timeout
     *
     * @var int
     */
    protected $timeout;
    /**
     * @var Serializer
     */
    protected $jms;

    /**
     * Client constructor.
     *
     * @param string $accessToken
     * @param string $baseUrl
     * @param int    $timeout
     */
    public function __construct(string $accessToken, string $baseUrl, int $timeout = 300)
    {
        $this->accessToken = $accessToken;
        $this->baseUrl = $baseUrl;
        $this->timeout = $timeout;

        /**
         * Annotations will be autoloaded implicitly.
         *
         * @deprecated will be removed in doctrine/annotations 2.0
         * @link       https://github.com/doctrine/annotations/issues/232
         */
        if (method_exists(AnnotationRegistry::class, 'registerLoader')) {
            AnnotationRegistry::registerLoader('class_exists');
        }

        $this->jms = SerializerBuilder::create()
            ->addDefaultHandlers()
//            ->setCacheDir(__DIR__ . '/runtime/cache') todo: fix tmp path
            ->build();
    }

    /**
     * NORDIC Data Sources covered by the nordic provider.
     * This method is better to use for the four nordic countries: Sweden, Denmark, Finland and Norway.
     *
     * PEP lists:
     * - PEP List (100% coverage on known PEP and RCA with Ssn and/or date of birth in Sweden.
     * Very good coverage on date of birth on known PEP and  RCA in Norway, Denmark, Finland and Iceland.)
     *
     * Sanction lists:
     * - United Nations Security Council Consolidated Sanctions List (UNSC)
     * - European Union Commission Consolidated List (EU)
     * - HM United Kingdom Treasury Consolidated List (UKT)
     *
     * @see https://docs.zignsec.com/api-2/watchlist-pep-sanctions/
     *
     * @param WatchlistNordicRequest $request
     *
     * @return WatchlistNordicResponse json provider's response
     * @throws ClientException
     */
    public function watchlistNordic(WatchlistNordicRequest $request): WatchlistNordicResponse
    {
        /** @var WatchlistNordicResponse $response */
        $response = $this->makeQuery(
            $request,
            Methods::getEndpoint($request->getMethod()),
            WatchlistNordicResponse::class
        );

        return $response;
    }

    /**
     * Global Data Sources covered by the global provider.
     *
     * PEP lists:
     * - A consolidated global PEP list, which is growing and updated on a daily basis.
     *
     * Sanction Lists:
     * - United Nations Sanctions (UN)
     * - US Consolidated Sanctions
     * - OFAC – Specially Designated Nationals (SDN)
     * - EU Financial Sanctions
     * - UK Financial Sanctions (HMT)
     * - Australian Sanctions
     * - Switzerland Sanction List – SECO
     * - INTERPOL Wanted List
     * - Consolidated Canadian Autonomous Sanctions List
     * - Office of the Superintendent of Financial Institutions (Canada)
     * - Bureau of Industry and Security (US)
     * - Department of State, AECA Debarred List (US)
     * - Department of State, Nonproliferation Sanctions (US)
     *
     * @see https://docs.zignsec.com/api-2/watchlist-pep-sanctions/
     *
     * @param WatchlistGlobalRequest $request
     *
     * @return WatchlistGlobalResponse json provider's response
     * @throws ClientException
     */
    public function watchlistGlobal(WatchlistGlobalRequest $request): WatchlistGlobalResponse
    {
        /** @var WatchlistGlobalResponse $response */
        $response = $this->makeQuery(
            $request,
            Methods::getEndpoint($request->getMethod()),
            WatchlistGlobalResponse::class
        );

        return $response;
    }

    /**
     * Create a new session to start using scanning.
     *
     * @see https://docs.zignsec.com/api-2/scanning-api/#step1
     *
     * @param CreateScanningSessionRequest $request
     *
     * @return CreateScanningSessionResponse
     * @throws ClientException
     */
    public function createScanningSession(CreateScanningSessionRequest $request): CreateScanningSessionResponse
    {
        /** @var CreateScanningSessionResponse $response */
        $response = $this->makeQuery(
            $request,
            Methods::getEndpoint($request->getMethod()),
            CreateScanningSessionResponse::class
        );

        return $response;
    }

    /**
     * Add documents to a scanning session.
     *
     * The supported binary image formats per the different Analysis types are:
     * - selfie: jpg/jpeg, png
     * - fraud: jpeg/jpg, pdf
     * - document: jpg/jpeg
     *
     * Note about the fraud detection: 98% of digital cameras natively take photos in JPG format – by analyzing those
     * natively we can get information about photo’s authenticity by analyzing traces about compression and
     * noise characteristics of the camera. PDF will only be fraud-analyzed if the source for the PDF was a JPG image.
     *
     * @see https://docs.zignsec.com/api-2/scanning-api/#step2
     *
     * @param AddDocsToScanningSessionRequest $request
     *
     * @return AddDocsToScanningSessionResponse
     * @throws ClientException
     */
    public function addDocsToScanningSession(AddDocsToScanningSessionRequest $request
    ): AddDocsToScanningSessionResponse {
        /** @var AddDocsToScanningSessionResponse $response */
        $response = $this->makeQuery(
            $request,
            str_replace(
                '{%scanningSessionId%}',
                $request->getScanningSessionId(),
                Methods::getEndpoint($request->getMethod())
            ),
            AddDocsToScanningSessionResponse::class,
            [
                'documentType' => $request->getDocumentType(),
                'reference'    => $request->getReference(),
            ],
            $request->getCurlFiles(),
        );

        return $response;
    }

    /**
     * Start scanning analysis.
     *
     * @see https://docs.zignsec.com/api-2/scanning-api/#step3
     *
     * @param StartDocsAnalysisRequest $request
     *
     * @return StartDocsAnalysisResponse
     * @throws ClientException
     */
    public function startDocsAnalysis(StartDocsAnalysisRequest $request): StartDocsAnalysisResponse
    {
        /** @var StartDocsAnalysisResponse $response */
        $response = $this->makeQuery(
            $request,
            str_replace(
                '{%scanningSessionId%}',
                $request->getScanningSessionId(),
                Methods::getEndpoint($request->getMethod())
            ),
            StartDocsAnalysisResponse::class,
            [
                'timeZoneOffset' => $request->getTimeZoneOffset(),
            ],
        );

        return $response;
    }

    /**
     * Get scanning analysis status.
     *
     * @see https://docs.zignsec.com/api-2/scanning-api/#step4
     *
     * @param GetDocsAnalysisStatusRequest $request
     *
     * @return GetDocsAnalysisStatusResponse
     * @throws ClientException
     */
    public function getDocsAnalysisStatus(GetDocsAnalysisStatusRequest $request): GetDocsAnalysisStatusResponse
    {
        /** @var GetDocsAnalysisStatusResponse $response */
        $response = $this->makeQuery(
            $request,
            str_replace(
                '{%scanningSessionId%}',
                $request->getScanningSessionId(),
                Methods::getEndpoint($request->getMethod())
            ),
            GetDocsAnalysisStatusResponse::class
        );

        return $response;
    }

    /**
     * Get scanning analysis results.
     *
     * A scanning session is the overarching unit of work. It contains a collection of documents to be analysed.
     * A scanning session can contain several analyses. An analysis is started by supplying the scanning session with
     * an analysis configuration object.
     * After starting analysis check analysis status or use webhook to get a notification when analysis is finished.
     * When it’s done results are available
     *
     * @see https://docs.zignsec.com/api-2/scanning-api/#step5
     *
     * @param GetDocsAnalysisResultsRequest $request
     *
     * @return GetDocsAnalysisResultsResponse
     * @throws ClientException
     */
    public function getDocsAnalysisResults(GetDocsAnalysisResultsRequest $request): GetDocsAnalysisResultsResponse
    {
        /** @var GetDocsAnalysisResultsResponse $response */
        $response = $this->makeQuery(
            $request,
            strtr(
                Methods::getEndpoint($request->getMethod()),
                [
                    '{%scanningSessionId%}' => $request->getScanningSessionId(),
                    '{%analysisId%}'        => $request->getAnalysisId(),
                ]
            ),
            GetDocsAnalysisResultsResponse::class
        );

        return $response;
    }

    /**
     * @param Request    $request
     * @param string     $endpoint
     * @param string     $responseClass
     * @param array      $queryParameters
     * @param CURLFile[] $files
     *
     * @return BaseResponse
     * @throws ClientException
     */
    protected function makeQuery(
        Request $request,
        string $endpoint,
        string $responseClass,
        array $queryParameters = [],
        array $files = []
    ): BaseResponse {
        $headers = [
            'Accept: application/json',
            'Authorization: ' . $this->accessToken,
        ];

        if (!empty($queryParameters)) {
            $endpoint .= '?' . http_build_query($queryParameters);
        }

        $ch = curl_init();

        if (empty($files)) {
            $rawRequest = $this->jms->serialize($request, 'json');
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_POSTFIELDS, $rawRequest);
        } else {
            $rawRequest = json_encode($this->jms->toArray($request), JSON_THROW_ON_ERROR);
            $headers[] = 'Content-Type: multipart/form-data';
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array_merge($this->jms->toArray($request), $files));
        }
        $url = $this->baseUrl . '/' . $endpoint;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request->getHttpMethod());
        curl_setopt($ch, CURLOPT_VERBOSE, true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true); // returns the request's headers in curl_getinfo()

        $curlResponse = curl_exec($ch);
        $this->checkResponse($ch, $curlResponse);

        try {
            /** @var BaseResponse $response */
            $response = $this->jms->deserialize($curlResponse, $responseClass, 'json');
            $response->setUrl($url);
            $response->setRawRequest($rawRequest);
            $response->setRawResponse((string) $curlResponse);
        } catch (Throwable $e) {
            throw new ClientException('Could not deserialize provider\'s response: ' . $e->getMessage());
        }

        return $response;
    }

    /**
     * @param $ch       - cURL handle
     * @param $response - cURL response
     *
     * @throws ClientException
     */
    protected function checkResponse($ch, $response): void
    {
        $curlLastErrorNumber = curl_errno($ch);
        if ($curlLastErrorNumber !== 0) {
            throw new ClientException('The last cURL error number: ' . $curlLastErrorNumber);
        }

        $curlInfo = curl_getinfo($ch);
        if (!isset($curlInfo['http_code'])) {
            throw new ClientException('The last cURL response has no \'http_code\' header');
        }

        $this->errorProcessing($response);
    }

    /**
     * @param string $response
     *
     * @throws ClientException
     */
    protected function errorProcessing(string $response): void
    {
        try {
            /** @var ErrorResponse $errorResponse */
            $errorResponse = $this->jms->deserialize($response, ErrorResponse::class, 'json');
            $firstError = $errorResponse->getErrors()[0] ?? null;
            if ($firstError instanceof Error) {
                throw new ClientException($firstError->getCode() . ": " . $firstError->getDescription());
            }
        } catch (ClientException $e) {
            throw $e;
        } catch (Throwable $e) {
            // В error message десериализовать не получилось, значит дальше пробуем собрать в success объект.
        }
    }
}
