<?php

namespace ffsoft\zignsec\messages\responses\watchlistNordic;

use ffsoft\zignsec\messages\responses\BaseResponse;
use ffsoft\zignsec\messages\responses\watchlist\Individuals;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class WatchlistNordicResponse
 *
 * @package ffsoft\zignsec\messages\responses\watchlistNordic
 */
class WatchlistNordicResponse extends BaseResponse
{
    /**
     * @SerializedName("id")
     * @Type("string")
     *
     * @var string
     */
    public $id;
    /**
     * @SerializedName("errors")
     * @Type("array")
     *
     * @var array
     */
    public $errors;
    /**
     * @SerializedName("HitCountTotal")
     * @Type("integer")
     *
     * @var int
     */
    public $hit_count_total;
    /**
     * @SerializedName("HitCountPEP")
     * @Type("integer")
     *
     * @var int
     */
    public $hit_count_pep;
    /**
     * @SerializedName("HitCountSanctions")
     * @Type("integer")
     *
     * @var int
     */
    public $hit_count_sanctions;
    /**
     * @SerializedName("_DataSource")
     * @Type("string")
     *
     * @var string
     */
    public $data_source;
    /**
     * @SerializedName("AttributeId")
     * @Type("string")
     *
     * @var string
     */
    public $attribute_id;
    /**
     * @SerializedName("Countries")
     * @Type("array")
     *
     * @var array
     */
    public $countries;
    /**
     * @SerializedName("Entities")
     * @Type("array")
     *
     * @var array
     */
    public $entities;
    /**
     * @SerializedName("Individuals")
     * @Type("array<ffsoft\zignsec\messages\responses\watchlist\Individuals>")
     *
     * @var Individuals[]
     */
    public $individuals;
    /**
     * @SerializedName("Name")
     * @Type("string")
     *
     * @var string
     */
    public $name;

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string[]
     */
    public function getErrors(): ?array
    {
        return $this->errors;
    }

    /**
     * @return int
     */
    public function getHitCountTotal(): ?int
    {
        return $this->hit_count_total;
    }

    /**
     * @return int
     */
    public function getHitCountPep(): ?int
    {
        return $this->hit_count_pep;
    }

    /**
     * @return int
     */
    public function getHitCountSanctions(): ?int
    {
        return $this->hit_count_sanctions;
    }

    /**
     * @return string
     */
    public function getDataSource(): ?string
    {
        return $this->data_source;
    }

    /**
     * @return string
     */
    public function getAttributeId(): ?string
    {
        return $this->attribute_id;
    }

    /**
     * @return string[]
     */
    public function getCountries(): ?array
    {
        return $this->countries;
    }

    /**
     * @return string[]
     */
    public function getEntities(): ?array
    {
        return $this->entities;
    }

    /**
     * @return Individuals[]
     */
    public function getIndividuals(): ?array
    {
        return $this->individuals;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }
}