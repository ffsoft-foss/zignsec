<?php

namespace ffsoft\zignsec\messages\responses\createScanningSession;

use ffsoft\zignsec\messages\responses\scanning\SessionResponse;

/**
 * Class CreateScanningSessionResponse
 *
 * @package ffsoft\zignsec\messages\responses\createScanningSession
 */
class CreateScanningSessionResponse extends SessionResponse
{
}
