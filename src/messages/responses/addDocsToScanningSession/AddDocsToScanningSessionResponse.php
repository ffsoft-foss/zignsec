<?php

namespace ffsoft\zignsec\messages\responses\addDocsToScanningSession;

use ffsoft\zignsec\messages\responses\scanning\SessionResponse;

/**
 * Class AddDocsToScanningSessionResponse
 *
 * @package ffsoft\zignsec\messages\responses\addDocsToScanningSession
 */
class AddDocsToScanningSessionResponse extends SessionResponse
{
}
