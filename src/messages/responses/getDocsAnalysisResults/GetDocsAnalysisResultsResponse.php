<?php

namespace ffsoft\zignsec\messages\responses\getDocsAnalysisResults;

use ffsoft\zignsec\enums\AnalysisStatuses;
use ffsoft\zignsec\messages\responses\BaseResponse;
use ffsoft\zignsec\messages\responses\scanning\AnalysisDetailsResponse;
use ffsoft\zignsec\messages\responses\scanning\AnalysisSummary;
use ffsoft\zignsec\messages\responses\scanning\Document;
use ffsoft\zignsec\messages\responses\scanning\Identity;
use ffsoft\zignsec\messages\responses\scanning\SessionParameters;
use ffsoft\zignsec\messages\responses\scanning\SessionResponse;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class GetDocsAnalysisResultsResponse
 *
 * @package ffsoft\zignsec\messages\responses\getDocsAnalysisResults
 */
class GetDocsAnalysisResultsResponse extends BaseResponse
{
    /**
     * @SerializedName("total_result")
     * @Type("string")
     * @see AnalysisStatuses
     * @var string|null
     */
    protected $totalResult;
    /**
     * @SerializedName("session_id")
     * @Type("string")
     * @see SessionResponse::$sessionId
     * @var string|null
     */
    protected $sessionId;
    /**
     * @SerializedName("start_date")
     * @Type("string")
     * @var string|null
     */
    protected $startDate;
    /**
     * @SerializedName("finish_date")
     * @Type("string")
     * @var string|null
     */
    protected $finishDate;
    /**
     * An id used on the scanning provider side.
     * @SerializedName("provider_folder_id")
     * @Type("string")
     *
     * @var string|null
     */
    protected $providerFolderId;
    /**
     * @SerializedName("request_parameters");
     * @Type("ffsoft\zignsec\messages\responses\scanning\SessionParameters")
     * @var SessionParameters|null
     */
    protected $requestParameters;
    /**
     * Identity (local values).
     * @SerializedName("identity");
     * @Type("ffsoft\zignsec\messages\responses\scanning\Identity")
     *
     * @var Identity|null
     */
    protected $identity;
    /**
     * Identity (English values).
     * @SerializedName("identity_english");
     * @Type("ffsoft\zignsec\messages\responses\scanning\Identity")
     *
     * @var Identity|null
     */
    protected $identityEnglish;
    /**
     * @SerializedName("analysis_summary");
     * @Type("ffsoft\zignsec\messages\responses\scanning\AnalysisSummary")
     * @var AnalysisSummary|null
     */
    protected $analysisSummary;
    /**
     * @SerializedName("analysis_details");
     * @Type("array<ffsoft\zignsec\messages\responses\scanning\AnalysisDetailsResponse>")
     * @var AnalysisDetailsResponse[]|null
     */
    protected $analysisDetails;
    /**
     * @SerializedName("documents")
     * @Type("array<ffsoft\zignsec\messages\responses\scanning\Document>")
     * @var Document[]|null
     */
    protected $documents;
    /**
     * @SerializedName("error")
     * @Type("string")
     * @var string|null
     */
    protected $error;

    /**
     * @return string|null
     */
    public function getTotalResult(): ?string
    {
        return $this->totalResult;
    }

    /**
     * @return string|null
     */
    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    /**
     * @return string|null
     */
    public function getStartDate(): ?string
    {
        return $this->startDate;
    }

    /**
     * @return string|null
     */
    public function getFinishDate(): ?string
    {
        return $this->finishDate;
    }

    /**
     * @return string|null
     */
    public function getProviderFolderId(): ?string
    {
        return $this->providerFolderId;
    }

    /**
     * @return SessionParameters|null
     */
    public function getRequestParameters(): ?SessionParameters
    {
        return $this->requestParameters;
    }

    /**
     * @return Identity|null
     */
    public function getIdentity(): ?Identity
    {
        return $this->identity;
    }

    /**
     * @return Identity|null
     */
    public function getIdentityEnglish(): ?Identity
    {
        return $this->identityEnglish;
    }

    /**
     * @return AnalysisSummary|null
     */
    public function getAnalysisSummary(): ?AnalysisSummary
    {
        return $this->analysisSummary;
    }

    /**
     * @return AnalysisDetailsResponse[]|null
     */
    public function getAnalysisDetails(): ?array
    {
        return $this->analysisDetails;
    }

    /**
     * @return Document[]|null
     */
    public function getDocuments(): ?array
    {
        return $this->documents;
    }

    /**
     * @return string|null
     */
    public function getError(): ?string
    {
        return $this->error;
    }
}
