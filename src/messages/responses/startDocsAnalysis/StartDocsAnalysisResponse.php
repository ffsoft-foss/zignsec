<?php

namespace ffsoft\zignsec\messages\responses\startDocsAnalysis;

use ffsoft\zignsec\messages\responses\scanning\AnalysisDetailsResponse;

/**
 * Class StartDocsAnalysisResponse
 *
 * @package ffsoft\zignsec\messages\responses\startDocsAnalysis
 */
class StartDocsAnalysisResponse extends AnalysisDetailsResponse
{
}
