<?php

namespace ffsoft\zignsec\messages\responses\getDocsAnalysisStatus;

use ffsoft\zignsec\enums\AnalysisStatuses;
use ffsoft\zignsec\messages\responses\BaseResponse;
use ffsoft\zignsec\messages\responses\scanning\SessionParameters;
use ffsoft\zignsec\messages\responses\scanning\SessionResponse;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class GetDocsAnalysisStatusResponse
 *
 * @package ffsoft\zignsec\messages\responses\getDocsAnalysisStatus
 */
class GetDocsAnalysisStatusResponse extends BaseResponse
{
    /**
     * В документации указано много дополнительных полей в этом респонсе. Но, во-первых, эти поля нам не нужны,
     * так как мы будем использовать данный респонс только для отслеживания статуса анализа, а во-вторых,
     * документация очень разнится с тем, что приходит на тестовой среде. Вопрос провайдеру был задан,
     * но отвечают они не часто и раз эти поля нам не особо интересны - десериализация этих полей происходить не будет.
     *
     * @see https://docs.zignsec.com/api-2/scanning-api/#step4
     */

    /**
     * @SerializedName("session_id");
     * @Type("string")
     *
     * @see SessionResponse::$sessionId
     *
     * @var string
     */
    protected $sessionId;
    /**
     * В документации указано поле "total_process_state", однако на деле приходит "session_status".
     * @SerializedName("session_status");
     * @Type("string")
     *
     * @see AnalysisStatuses
     *
     * @var string
     */
    protected $sessionStatus;
    /**
     * @SerializedName("session_parameters");
     * @Type("ffsoft\zignsec\messages\responses\scanning\SessionParameters")
     * @var SessionParameters|null
     */
    protected $sessionParameters;

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * @return string
     */
    public function getSessionStatus(): string
    {
        return $this->sessionStatus;
    }

    /**
     * @return SessionParameters|null
     */
    public function getSessionParameters(): ?SessionParameters
    {
        return $this->sessionParameters;
    }
}
