<?php

namespace ffsoft\zignsec\messages\responses\watchlist;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

class BirthDate
{
    /**
     * @SerializedName("Date")
     * @Type("string")
     * @var string
     */
    public $date;
    /**
     * @SerializedName("Note")
     * @Type("string")
     *
     * @var string
     */
    public $note;

    /**
     * @return null|string
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @return null|string
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
}