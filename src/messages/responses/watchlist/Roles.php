<?php

namespace ffsoft\zignsec\messages\responses\watchlist;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

class Roles
{
    /**
     * @SerializedName("Title")
     * @Type("string")
     *
     * @var string
     */
    public $title;
    /**
     * @SerializedName("Category")
     * @Type("string")
     *
     * @var string
     */
    public $category;
    /**
     * @SerializedName("Since")
     * @Type("string")
     *
     * @var string
     */
    public $since;
    /**
     * @SerializedName("To")
     * @Type("string")
     *
     * @var string
     */
    public $to;
    /**
     * @SerializedName("Type")
     * @Type("string")
     *
     * @var string
     */
    public $type;

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return null|string
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @return null|string
     */
    public function getSince(): ?string
    {
        return $this->since;
    }

    /**
     * @return null|string
     */
    public function getTo(): ?string
    {
        return $this->to;
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type;
    }
}