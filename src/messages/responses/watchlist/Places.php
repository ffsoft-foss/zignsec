<?php

namespace ffsoft\zignsec\messages\responses\watchlist;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

class Places
{
    /**
     * @SerializedName("Country")
     * @Type("string")
     *
     * @var string
     */
    public $country;
    /**
     * @SerializedName("Location")
     * @Type("string")
     *
     * @var string
     */
    public $location;
    /**
     * @SerializedName("Type")
     * @Type("string")
     *
     * @var string
     */
    public $type;

    /**
     * @return null|string
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @return null|string
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type;
    }
}