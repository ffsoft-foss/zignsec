<?php

namespace ffsoft\zignsec\messages\responses\watchlist;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

class Links
{
    /**
     * @SerializedName("Url")
     * @Type("string")
     *
     * @var string
     */
    public $url;
    /**
     * @SerializedName("Type")
     * @Type("string")
     *
     * @var string
     */
    public $type;

    /**
     * @return null|string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type;
    }
}