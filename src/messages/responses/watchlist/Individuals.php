<?php

namespace ffsoft\zignsec\messages\responses\watchlist;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

class Individuals
{
    /**
     * @SerializedName("Addresses")
     * @Type("array")
     *
     * @var array
     */
    public $addresses;
    /**
     * @SerializedName("Aliases")
     * @Type("array")
     *
     * @var array
     */
    public $aliases;
    /**
     * @SerializedName("AttributeId")
     * @Type("string")
     *
     * @var string
     */
    public $attributeId;
    /**
     * @SerializedName("BirthDate")
     * @Type("string")
     *
     * @var string
     */
    public $birthDate;
    /**
     * @SerializedName("comment")
     * @Type("string")
     *
     * @var string
     */
    public $comment;
    /**
     * @SerializedName("Engagements")
     * @Type("array")
     *
     * @var array
     */
    public $engagements;
    /**
     * @SerializedName("ExternalId")
     * @Type("string")
     *
     * @var string
     */
    public $externalId;
    /**
     * @SerializedName("ExternalUrls")
     * @Type("array")
     *
     * @var array
     */
    public $externalUrls;
    /**
     * @SerializedName("FunctionDescription")
     * @Type("string")
     *
     * @var string
     */
    public $functionDescription;
    /**
     * @SerializedName("HitRating")
     * @Type("integer")
     *
     * @var int
     */
    public $HitRating;
    /**
     * @SerializedName("Id")
     * @Type("integer")
     *
     * @var int
     */
    public $id;
    /**
     * @SerializedName("IsMale")
     * @Type("boolean")
     *
     * @var bool
     */
    public $isMale;
    /**
     * @SerializedName("KeyWordHitRating")
     * @Type("float")
     *
     * @var float
     */
    public $keyWordHitRating;
    /**
     * @SerializedName("LastUpdate")
     * @Type("string")
     *
     * @var string
     */
    public $lastUpdate;
    /**
     * @SerializedName("ListType")
     * @Type("string")
     *
     * @var string
     */
    public $listType;
    /**
     * @SerializedName("Name")
     * @Type("string")
     *
     * @var string
     */
    public $name;
    /**
     * @SerializedName("OriginalBirthDate")
     * @Type("string")
     *
     * @var string
     */
    public $originalBirthDate;
    /**
     * @SerializedName("PostDate")
     * @Type("string")
     *
     * @var string
     */
    public $postDate;
    /**
     * @SerializedName("sourceName")
     * @Type("string")
     *
     * @var string
     */
    public $SourceName;
    /**
     * @SerializedName("Ssn")
     * @Type("string")
     *
     * @var string
     */
    public $ssn;
    /**
     * @SerializedName("Title")
     * @Type("string")
     *
     * @var string
     */
    public $title;
    /**
     * @SerializedName("Url")
     * @Type("string")
     *
     * @var string
     */
    public $url;
    /**
     * @SerializedName("Xml")
     * @Type("string")
     *
     * @var string
     */
    public $xml;

    /**
     * @return null|string[]
     */
    public function getAddresses(): ?array
    {
        return $this->addresses;
    }

    /**
     * @return null|string[]
     */
    public function getAliases(): ?array
    {
        return $this->aliases;
    }

    /**
     * @return null|string
     */
    public function getAttributeId(): ?string
    {
        return $this->attributeId;
    }

    /**
     * @return null|string
     */
    public function getBirthDate(): ?string
    {
        return $this->birthDate;
    }

    /**
     * @return null|string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return null|string[]
     */
    public function getEngagements(): ?array
    {
        return $this->engagements;
    }

    /**
     * @return null|string
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @return null|string[]
     */
    public function getExternalUrls(): ?array
    {
        return $this->externalUrls;
    }

    /**
     * @return null|string
     */
    public function getFunctionDescription(): ?string
    {
        return $this->functionDescription;
    }

    /**
     * @return null|int
     */
    public function getHitRating(): ?int
    {
        return $this->HitRating;
    }

    /**
     * @return null|int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return null|bool
     */
    public function isMale(): ?bool
    {
        return $this->isMale;
    }

    /**
     * @return null|float
     */
    public function getKeyWordHitRating(): ?float
    {
        return $this->keyWordHitRating;
    }

    /**
     * @return null|string
     */
    public function getLastUpdate(): ?string
    {
        return $this->lastUpdate;
    }

    /**
     * @return null|string
     */
    public function getListType(): ?string
    {
        return $this->listType;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function getOriginalBirthDate(): ?string
    {
        return $this->originalBirthDate;
    }

    /**
     * @return null|string
     */
    public function getPostDate(): ?string
    {
        return $this->postDate;
    }

    /**
     * @return null|string
     */
    public function getSourceName(): ?string
    {
        return $this->SourceName;
    }

    /**
     * @return null|string
     */
    public function getSsn(): ?string
    {
        return $this->ssn;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return null|string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return null|string
     */
    public function getXml(): ?string
    {
        return $this->xml;
    }
}