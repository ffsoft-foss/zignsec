<?php

namespace ffsoft\zignsec\messages\responses\watchlist;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class Persons
 *
 * @package ffsoft\zignsec\messages\responses\watchlist
 */
class Persons
{
    /**
     * @SerializedName("Update_At")
     * @Type("string")
     *
     * @var string
     */
    public $update_at;
    /**
     * @SerializedName("Category")
     * @Type("string")
     *
     * @var string
     */
    public $category;
    /**
     * @SerializedName("Name")
     * @Type("string")
     *
     * @var string
     */
    public $name;
    /**
     * @SerializedName("First_Name")
     * @Type("string")
     *
     * @var string
     */
    public $first_name;
    /**
     * @SerializedName("Last_Name")
     * @Type("string")
     *
     * @var string
     */
    public $last_name;
    /**
     * @SerializedName("Gender")
     * @Type("string")
     *
     * @var string
     */
    public $gender;
    /**
     * @SerializedName("Dates_Of_Birth")
     * @Type("array<ffsoft\zignsec\messages\responses\watchlist\BirthDate>")
     *
     * @var BirthDate[]
     */
    public $dates_of_birth;
    /**
     * @SerializedName("Places_Of_Birth")
     * @Type("array<ffsoft\zignsec\messages\responses\watchlist\Places>")
     *
     * @var /ffsoft\zignsec\messages\responses\watchlist\Places[]
     */
    public $places_of_birth;
    /**
     * @SerializedName("Reference_Type")
     * @Type("string")
     *
     * @var string
     */
    public $reference_type;
    /**
     * @SerializedName("Nationality")
     * @Type("string")
     *
     * @var string
     */
    public $nationality;
    /**
     * @SerializedName("Places")
     * @Type("array<ffsoft\zignsec\messages\responses\watchlist\Places>")
     *
     * @var Places[]
     */
    public $places;
    /**
     * @SerializedName("Other_Names")
     * @Type("array<ffsoft\zignsec\messages\responses\watchlist\OtherNames>")
     *
     * @var OtherNames[]
     */
    public $other_names;
    /**
     * @SerializedName("Roles")
     * @Type("array<ffsoft\zignsec\messages\responses\watchlist\Roles>")
     *
     * @var Roles[]
     */
    public $roles;
    /**
     * @SerializedName("Occupations")
     * @Type("array")
     *
     * @var array
     */
    public $occupations;
    /**
     * @SerializedName("Children")
     * @Type("array")
     *
     * @var array
     */
    public $children;
    /**
     * @SerializedName("Siblings")
     * @Type("array")
     *
     * @var array
     */
    public $siblings;
    /**
     * @SerializedName("Images")
     * @Type("array")
     *
     * @var array
     */
    public $images;
    /**
     * @SerializedName("Contacts")
     * @Type("array")
     *
     * @var array
     */
    public $contacts;
    /**
     * @SerializedName("Links")
     * @Type("array<ffsoft\zignsec\messages\responses\watchlist\Links>")
     *
     * @var Links[]
     */
    public $links;
    /**
     * @SerializedName("Political_Parties")
     * @Type("array")
     *
     * @var array
     */
    public $political_parties;
    /**
     * @SerializedName("Father")
     * @Type("string")
     *
     * @var string
     */
    public $father;
    /**
     * @SerializedName("Mother")
     * @Type("string")
     *
     * @var string
     */
    public $mother;
    /**
     * @SerializedName("Spouse")
     * @Type("string")
     *
     * @var string
     */
    public $spouse;
    /**
     * @SerializedName("Summary")
     * @Type("string")
     *
     * @var string
     */
    public $summary;
    /**
     * @SerializedName("Match_Rate")
     * @Type("string")
     *
     * @var string
     */
    public $match_rate;

    /**
     * @return null|string
     */
    public function getUpdateAt(): ?string
    {
        return $this->update_at;
    }

    /**
     * @return null|string
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    /**
     * @return null|string
     */
    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    /**
     * @return null|string
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @return null|BirthDate[]
     */
    public function getDatesOfBirth(): ?array
    {
        return $this->dates_of_birth;
    }

    /**
     * @return null|string[]
     */
    public function getPlacesOfBirth(): ?array
    {
        return $this->places_of_birth;
    }

    /**
     * @return null|string
     */
    public function getReferenceType(): ?string
    {
        return $this->reference_type;
    }

    /**
     * @return null|string
     */
    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    /**
     * @return null|Places[]
     */
    public function getPlaces(): ?array
    {
        return $this->places;
    }

    /**
     * @return null|OtherNames[]
     */
    public function getOtherNames(): ?array
    {
        return $this->other_names;
    }

    /**
     * @return null|Roles[]
     */
    public function getRoles(): ?array
    {
        return $this->roles;
    }

    /**
     * @return null|string[]
     */
    public function getOccupations(): ?array
    {
        return $this->occupations;
    }

    /**
     * @return null|string[]
     */
    public function getChildren(): ?array
    {
        return $this->children;
    }

    /**
     * @return null|string[]
     */
    public function getSiblings(): ?array
    {
        return $this->siblings;
    }

    /**
     * @return null|string[]
     */
    public function getImages(): ?array
    {
        return $this->images;
    }

    /**
     * @return null|string[]
     */
    public function getContacts(): ?array
    {
        return $this->contacts;
    }

    /**
     * @return null|Links[]
     */
    public function getLinks(): ?array
    {
        return $this->links;
    }

    /**
     * @return null|string[]
     */
    public function getPoliticalParties(): ?array
    {
        return $this->political_parties;
    }

    /**
     * @return null|string
     */
    public function getFather(): ?string
    {
        return $this->father;
    }

    /**
     * @return null|string
     */
    public function getMother(): ?string
    {
        return $this->mother;
    }

    /**
     * @return null|string
     */
    public function getSpouse(): ?string
    {
        return $this->spouse;
    }

    /**
     * @return null|string
     */
    public function getSummary(): ?string
    {
        return $this->summary;
    }

    /**
     * @return null|string
     */
    public function getMatchRate(): ?string
    {
        return $this->match_rate;
    }
}