<?php

namespace ffsoft\zignsec\messages\responses\watchlist;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

class OtherNames
{
    /**
     * @SerializedName("Name")
     * @Type("string")
     *
     * @var string
     */
    public $name;
    /**
     * @SerializedName("Type")
     * @Type("string")
     *
     * @var string
     */
    public $type;

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type;
    }
}