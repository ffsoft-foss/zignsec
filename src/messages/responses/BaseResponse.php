<?php

namespace ffsoft\zignsec\messages\responses;

use JMS\Serializer\Annotation as JMS;

abstract class BaseResponse
{
    /**
     * @var string
     * @JMS\Exclude
     */
    public $url;

    /**
     * @var string|null
     * @JMS\Exclude
     */
    public $rawRequest;

    /**
     * @var string
     * @JMS\Exclude
     */
    public $rawResponse;

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return string|null
     */
    public function getRawRequest(): ?string
    {
        return $this->rawRequest;
    }

    /**
     * @param string|null $rawRequest
     */
    public function setRawRequest(?string $rawRequest): void
    {
        $this->rawRequest = $rawRequest;
    }

    /**
     * @return string
     */
    public function getRawResponse(): string
    {
        return $this->rawResponse;
    }

    /**
     * @param string $rawResponse
     */
    public function setRawResponse(string $rawResponse): void
    {
        $this->rawResponse = $rawResponse;
    }


}
