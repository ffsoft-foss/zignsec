<?php

namespace ffsoft\zignsec\messages\responses\scanning;

use ffsoft\zignsec\enums\AnalysisTypes;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

class SessionParameters
{
    /**
     * State sent from the client persisted in session. Is sent back to redirect urls and webhooks as is.
     * @SerializedName("relay_state")
     * @Type("string")
     *
     * @var string|null
     */
    protected $relayState;
    /**
     * Session type Api | BrowserFlow | WebSdk.
     * Не стал заводить ENUM, так как мы пользуемся только API (+на тестовой среде провайдер вообще "0" отдаёт).
     * @SerializedName("session_type")
     * @Type("string")
     *
     * @var string|null
     */
    protected $sessionType;
    /**
     * @SerializedName("webhook")
     * @Type("string")
     * @var string|null
     */
    protected $webhook;
    /**
     * To be used by the Browser Flow to redirect user when analysis is finished.
     * @SerializedName("target")
     * @Type("string")
     *
     * @var string|null
     */
    protected $target;
    /**
     * To be used by Browser Flow to redirect user when analysis is FAILED.
     * @SerializedName("target_error")
     * @Type("string")
     *
     * @var string|null
     */
    protected $targetError;
    /**
     * @SerializedName("analysis_types")
     * @Type("array")
     * @see AnalysisTypes
     * @var array|null
     */
    protected $analysisTypes;

    /**
     * @return string|null
     */
    public function getRelayState(): ?string
    {
        return $this->relayState;
    }

    /**
     * @return string|null
     */
    public function getSessionType(): ?string
    {
        return $this->sessionType;
    }

    /**
     * @return string|null
     */
    public function getWebhook(): ?string
    {
        return $this->webhook;
    }

    /**
     * @return string|null
     */
    public function getTarget(): ?string
    {
        return $this->target;
    }

    /**
     * @return string|null
     */
    public function getTargetError(): ?string
    {
        return $this->targetError;
    }

    /**
     * @return array|null
     */
    public function getAnalysisTypes(): ?array
    {
        return $this->analysisTypes;
    }
}
