<?php

namespace ffsoft\zignsec\messages\responses\scanning;

use ffsoft\zignsec\enums\AnalysisStatuses;
use ffsoft\zignsec\enums\FraudAnalysisRiskScores;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class FraudAnalysisSummary
 *
 * @package ffsoft\zignsec\messages\responses\scanning
 */
class FraudAnalysisSummary
{
    /**
     * @SerializedName("result")
     * @Type("string")
     * @see AnalysisStatuses
     * @var string
     */
    protected $result;
    /**
     * Result string representation.
     * @SerializedName("result_title")
     * @Type("string")
     *
     * @var string|null
     */
    protected $resultTitle;
    /**
     * Reason behind the result (usually – for DECLINED sessions).
     * @SerializedName("result_reason")
     * @Type("string")
     *
     * @var string|null
     */
    protected $resultReason;
    /**
     * All errors as a text.
     * @SerializedName("error")
     * @Type("string")
     *
     * @var string|null
     */
    protected $error;
    /**
     * All errors from fraud analysis (see details to get errors for each particular image).
     * @SerializedName("all_errors")
     * @Type("array")
     *
     * @var array|null
     */
    protected $allErrors;
    /**
     * @SerializedName("risk_score")
     * @Type("string")
     * @see FraudAnalysisRiskScores
     * @var string|null
     */
    protected $riskScore;

    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * @return string|null
     */
    public function getResultTitle(): ?string
    {
        return $this->resultTitle;
    }

    /**
     * @return string|null
     */
    public function getResultReason(): ?string
    {
        return $this->resultReason;
    }

    /**
     * @return string|null
     */
    public function getError(): ?string
    {
        return $this->error;
    }

    /**
     * @return array|null
     */
    public function getAllErrors(): ?array
    {
        return $this->allErrors;
    }

    /**
     * @return string|null
     */
    public function getRiskScore(): ?string
    {
        return $this->riskScore;
    }
}
