<?php

namespace ffsoft\zignsec\messages\responses\scanning;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Описание этой модели в документации отсутствует. Но это по сути самая главная модель для нас,
 * так как она содержит все поля, которые спарсил провайдер с документа. Поэтому пришлось поля присутствующие здесь
 * вписывались согласно тому, что возвращала тестовая среда провайдера.
 *
 * Class DocumentFields
 *
 * @package ffsoft\zignsec\messages\responses\scanning
 */
class DocumentFields
{
    /**
     * First Name
     * @SerializedName("Given_Names");
     * @Type("string")
     *
     * @var string|null
     */
    protected $givenNames;
    /**
     * Last Name
     * @SerializedName("Surname");
     * @Type("string")
     *
     * @var string|null
     */
    protected $surname;
    /**
     * Issuer country name
     * @SerializedName("Issuing_State_Name");
     * @Type("string")
     *
     * @var string|null
     */
    protected $issuingStateName;
    /**
     * Issuer country code (ISO3)
     * @SerializedName("Issuing_State_Code");
     * @Type("string")
     *
     * @var string|null
     */
    protected $issuingStateCode;
    /**
     * Identity document number
     * @SerializedName("Document_Number");
     * @Type("string")
     *
     * @var string|null
     */
    protected $documentNumber;
    /**
     * Identity document personal number
     * @SerializedName("Personal_Number");
     * @Type("string")
     *
     * @var string|null
     */
    protected $personalNumber;
    /**
     * Issuer
     * @SerializedName("Authority");
     * @Type("string")
     *
     * @var string|null
     */
    protected $authority;
    /**
     * Issuer
     * @SerializedName("Date_of_Birth");
     * @Type("string")
     *
     * @var string|null
     */
    protected $dateOfBirth;
    /**
     * Issue date
     * @SerializedName("Date_of_Issue");
     * @Type("string")
     *
     * @var string|null
     */
    protected $dateOfIssue;
    /**
     * Valid date
     * @SerializedName("Date_of_Expiry");
     * @Type("string")
     *
     * @var string|null
     */
    protected $dateOfExpiry;
    /**
     * Birth place
     * @SerializedName("Place_of_Birth");
     * @Type("string")
     *
     * @var string|null
     */
    protected $placeOfBirth;

    /**
     * @return string|null
     */
    public function getGivenNames(): ?string
    {
        return $this->givenNames;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @return string|null
     */
    public function getIssuingStateName(): ?string
    {
        return $this->issuingStateName;
    }

    /**
     * @return string|null
     */
    public function getIssuingStateCode(): ?string
    {
        return $this->issuingStateCode;
    }

    /**
     * @return string|null
     */
    public function getDocumentNumber(): ?string
    {
        return $this->documentNumber;
    }

    /**
     * @return string|null
     */
    public function getPersonalNumber(): ?string
    {
        return $this->personalNumber;
    }

    /**
     * @return string|null
     */
    public function getAuthority(): ?string
    {
        return $this->authority;
    }

    /**
     * @return string|null
     */
    public function getDateOfBirth(): ?string
    {
        return $this->dateOfBirth;
    }

    /**
     * @return string|null
     */
    public function getDateOfIssue(): ?string
    {
        return $this->dateOfIssue;
    }

    /**
     * @return string|null
     */
    public function getDateOfExpiry(): ?string
    {
        return $this->dateOfExpiry;
    }

    /**
     * @return string|null
     */
    public function getPlaceOfBirth(): ?string
    {
        return $this->placeOfBirth;
    }
}
