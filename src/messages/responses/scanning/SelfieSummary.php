<?php

namespace ffsoft\zignsec\messages\responses\scanning;

use ffsoft\zignsec\enums\AnalysisStatuses;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class SelfieSummary
 *
 * @package ffsoft\zignsec\messages\responses\scanning
 */
class SelfieSummary
{
    /**
     * @SerializedName("result")
     * @Type("string")
     * @see AnalysisStatuses
     * @var string
     */
    protected $result;
    /**
     * Result string representation.
     * @SerializedName("result_title")
     * @Type("string")
     *
     * @var string|null
     */
    protected $resultTitle;
    /**
     * Reason behind the result (usually – for DECLINED sessions).
     * @SerializedName("result_reason")
     * @Type("string")
     *
     * @var string|null
     */
    protected $resultReason;
    /**
     * All errors as a text.
     * @SerializedName("error")
     * @Type("string")
     *
     * @var string|null
     */
    protected $error;
    /**
     * Min confidence in the selfie match.
     * @SerializedName("min_confidence")
     * @Type("string")
     *
     * @var string|null
     */
    protected $minConfidence;
    /**
     * Max confidence in the selfie match.
     * @SerializedName("max_confidence")
     * @Type("string")
     *
     * @var string|null
     */
    protected $maxConfidence;

    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * @return string|null
     */
    public function getResultTitle(): ?string
    {
        return $this->resultTitle;
    }

    /**
     * @return string|null
     */
    public function getResultReason(): ?string
    {
        return $this->resultReason;
    }

    /**
     * @return string|null
     */
    public function getError(): ?string
    {
        return $this->error;
    }

    /**
     * @return string|null
     */
    public function getMinConfidence(): ?string
    {
        return $this->minConfidence;
    }

    /**
     * @return string|null
     */
    public function getMaxConfidence(): ?string
    {
        return $this->maxConfidence;
    }
}
