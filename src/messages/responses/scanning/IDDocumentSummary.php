<?php

namespace ffsoft\zignsec\messages\responses\scanning;

use ffsoft\zignsec\enums\AnalysisStatuses;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class IDDocumentSummary
 *
 * @package ffsoft\zignsec\messages\responses\scanning
 */
class IDDocumentSummary
{
    /**
     * @SerializedName("result")
     * @Type("string")
     * @see AnalysisStatuses
     * @var string|null
     */
    protected $result;
    /**
     * Result string representation.
     * @SerializedName("result_title")
     * @Type("string")
     *
     * @var string|null
     */
    protected $resultTitle;
    /**
     * Reason behind the result (usually – for DECLINED sessions).
     * @SerializedName("result_reason")
     * @Type("string")
     *
     * @example "RemainderTerm,Address_local,Date_of_Expiry" - понимай как хочешь, но что-то с этими полями не так.
     * @var string|null
     */
    protected $resultReason;
    /**
     * All errors as a text.
     * @SerializedName("error")
     * @Type("string")
     *
     * @var string|null
     */
    protected $error;
    /**
     * A document type name of the first found document.
     * @SerializedName("document_type_name")
     * @Type("string")
     *
     * @example "Ukraine - ePassport (2015)"
     * @var string|null
     */
    protected $documentTypeName;
    /**
     * @SerializedName("first_name_english")
     * @Type("string")
     * @var string|null
     */
    protected $firstNameEnglish;
    /**
     * @SerializedName("last_name_english")
     * @Type("string")
     * @var string|null
     */
    protected $lastNameEnglish;
    /**
     * @SerializedName("full_name_english")
     * @Type("string")
     * @var string|null
     */
    protected $fullNameEnglish;
    /**
     * @SerializedName("age")
     * @Type("string")
     * @var string|null
     */
    protected $age;
    /**
     * @SerializedName("expiration_date")
     * @Type("string")
     * @var string|null
     */
    protected $expirationDate;
    /**
     * @SerializedName("issuer_state")
     * @Type("string")
     * @var string|null
     */
    protected $issuerState;
    /**
     * @SerializedName("fields")
     * @Type("ffsoft\zignsec\messages\responses\scanning\DocumentFields")
     * @var DocumentFields|null
     */
    protected $fields;

    /**
     * @return string|null
     */
    public function getResult(): ?string
    {
        return $this->result;
    }

    /**
     * @return string|null
     */
    public function getResultTitle(): ?string
    {
        return $this->resultTitle;
    }

    /**
     * @return string|null
     */
    public function getResultReason(): ?string
    {
        return $this->resultReason;
    }

    /**
     * @return string|null
     */
    public function getError(): ?string
    {
        return $this->error;
    }

    /**
     * @return string|null
     */
    public function getDocumentTypeName(): ?string
    {
        return $this->documentTypeName;
    }

    /**
     * @return string|null
     */
    public function getFirstNameEnglish(): ?string
    {
        return $this->firstNameEnglish;
    }

    /**
     * @return string|null
     */
    public function getLastNameEnglish(): ?string
    {
        return $this->lastNameEnglish;
    }

    /**
     * @return string|null
     */
    public function getFullNameEnglish(): ?string
    {
        return $this->fullNameEnglish;
    }

    /**
     * @return string|null
     */
    public function getAge(): ?string
    {
        return $this->age;
    }

    /**
     * @return string|null
     */
    public function getExpirationDate(): ?string
    {
        return $this->expirationDate;
    }

    /**
     * @return string|null
     */
    public function getIssuerState(): ?string
    {
        return $this->issuerState;
    }

    /**
     * @return DocumentFields|null
     */
    public function getFields(): ?DocumentFields
    {
        return $this->fields;
    }
}
