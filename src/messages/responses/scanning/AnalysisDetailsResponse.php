<?php

namespace ffsoft\zignsec\messages\responses\scanning;

use ffsoft\zignsec\enums\AnalysisStates;
use ffsoft\zignsec\enums\AnalysisStatuses;
use ffsoft\zignsec\messages\responses\BaseResponse;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class AnalysisDetailsResponse
 *
 * @package ffsoft\zignsec\messages\responses\scanning
 */
class AnalysisDetailsResponse extends BaseResponse
{
    /**
     * An analysis identifier.
     * @SerializedName("analysis_id")
     * @Type("string")
     *
     * @var string|null
     */
    protected $analysisId;
    /**
     * Analysis start time.
     * @SerializedName("started_at")
     * @Type("string")
     *
     * @var string|null
     */
    protected $startedAt;
    /**
     * Analysis finish time.
     * @SerializedName("finished_at")
     * @Type("string")
     *
     * @var string|null
     */
    protected $finishedAt;
    /**
     * The total process state for all analyses. Indicated whether the analyses are finished or not.
     * @SerializedName("total_process_state")
     * @Type("string")
     *
     * @see AnalysisStates
     * @var string|null
     */
    protected $totalProcessState;
    /**
     * @SerializedName("document_analysis_state")
     * @Type("string")
     * @see AnalysisStates
     * @var string|null
     */
    protected $documentAnalysisState;
    /**
     * @SerializedName("document_analysis_result")
     * @Type("string")
     * @see AnalysisStatuses
     * @var string|null
     */
    protected $documentAnalysisResult;
    /**
     * Failed fields from the document analysis if any (CSV format).
     * @SerializedName("document_analysis_failed_fields")
     * @Type("string")
     *
     * @var string|null
     */
    protected $documentAnalysisFailedFields;
    /**
     * @SerializedName("document_analysis_details")
     * @Type("array")
     * @var array|null
     */
    protected $documentAnalysisDetails;
    /**
     * @SerializedName("fraud_analysis_state")
     * @Type("string")
     * @see AnalysisStates
     * @var string|null
     */
    protected $fraudAnalysisState;
    /**
     * @SerializedName("fraud_analysis_result")
     * @Type("string")
     * @see AnalysisStatuses
     * @var string|null
     */
    protected $fraudAnalysisResult;
    /**
     * @SerializedName("fraud_analysis_risk_score")
     * @Type("string")
     * @see FraudAnalysisRiskScores
     * @var string|null
     */
    protected $fraudAnalysisRiskScore;
    /**
     * @SerializedName("expert_analysis_state")
     * @Type("string")
     * @see AnalysisStates
     * @var string|null
     */
    protected $expertAnalysisState;
    /**
     * @SerializedName("expert_analysis_result")
     * @Type("string")
     * @see AnalysisStatuses
     * @var string|null
     */
    protected $expertAnalysisResult;
    /**
     * Identity filled from the given document , contains local values.
     * @SerializedName("identity")
     * @Type("ffsoft\zignsec\messages\responses\scanning\Identity")
     *
     * @var Identity|null
     */
    protected $identity;
    /**
     * English version of the identity.
     * @SerializedName("identity_english")
     * @Type("ffsoft\zignsec\messages\responses\scanning\Identity")
     *
     * @var Identity|null
     */
    protected $identityEnglish;
    /**
     * @SerializedName("analysed_documents")
     * @Type("array<ffsoft\zignsec\messages\responses\scanning\Document>")
     * @var Document[]|null
     */
    protected $analysedDocuments;

    /**
     * @return string|null
     */
    public function getAnalysisId(): ?string
    {
        return $this->analysisId;
    }

    /**
     * @return string|null
     */
    public function getStartedAt(): ?string
    {
        return $this->startedAt;
    }

    /**
     * @return string|null
     */
    public function getFinishedAt(): ?string
    {
        return $this->finishedAt;
    }

    /**
     * @return string|null
     */
    public function getTotalProcessState(): ?string
    {
        return $this->totalProcessState;
    }

    /**
     * @return string|null
     */
    public function getDocumentAnalysisState(): ?string
    {
        return $this->documentAnalysisState;
    }

    /**
     * @return string|null
     */
    public function getDocumentAnalysisResult(): ?string
    {
        return $this->documentAnalysisResult;
    }

    /**
     * @return string|null
     */
    public function getDocumentAnalysisFailedFields(): ?string
    {
        return $this->documentAnalysisFailedFields;
    }

    /**
     * @return array|null
     */
    public function getDocumentAnalysisDetails(): ?array
    {
        return $this->documentAnalysisDetails;
    }

    /**
     * @return string|null
     */
    public function getFraudAnalysisState(): ?string
    {
        return $this->fraudAnalysisState;
    }

    /**
     * @return string|null
     */
    public function getFraudAnalysisResult(): ?string
    {
        return $this->fraudAnalysisResult;
    }

    /**
     * @return string|null
     */
    public function getFraudAnalysisRiskScore(): ?string
    {
        return $this->fraudAnalysisRiskScore;
    }

    /**
     * @return string|null
     */
    public function getExpertAnalysisState(): ?string
    {
        return $this->expertAnalysisState;
    }

    /**
     * @return string|null
     */
    public function getExpertAnalysisResult(): ?string
    {
        return $this->expertAnalysisResult;
    }

    /**
     * @return Identity|null
     */
    public function getIdentity(): ?Identity
    {
        return $this->identity;
    }

    /**
     * @return Identity|null
     */
    public function getIdentityEnglish(): ?Identity
    {
        return $this->identityEnglish;
    }

    /**
     * @return Document[]|null
     */
    public function getAnalysedDocuments(): ?array
    {
        return $this->analysedDocuments;
    }
}
