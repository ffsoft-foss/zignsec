<?php

namespace ffsoft\zignsec\messages\responses\scanning;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class Document
 *
 * @package ffsoft\zignsec\messages\responses\scanning
 */
class Document
{
    /**
     * @SerializedName("id")
     * @Type("integer")
     * @var int
     */
    public $id;
    /**
     * @SerializedName("file_name")
     * @Type("string")
     * @var string
     */
    public $fileName;
    /**
     * @SerializedName("image_url")
     * @Type("string")
     * @var string
     */
    public $imageUrl;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }
}
