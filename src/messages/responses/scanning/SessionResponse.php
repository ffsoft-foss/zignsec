<?php

namespace ffsoft\zignsec\messages\responses\scanning;

use ffsoft\zignsec\messages\responses\BaseResponse;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class SessionResponse
 *
 * @package ffsoft\zignsec\messages\responses\scanning
 */
class SessionResponse extends BaseResponse
{
    /**
     * @SerializedName("session_id")
     * @Type("string")
     * @var string
     */
    public $sessionId;
    /**
     * @SerializedName("provider_folder_id")
     * @Type("string")
     * @var string
     */
    public $providerFolderId;
    /**
     * @SerializedName("documents")
     * @Type("array<ffsoft\zignsec\messages\responses\scanning\Document>")
     * @var Document[]|null
     */
    public $documents;

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * @return string
     */
    public function getProviderFolderId(): string
    {
        return $this->providerFolderId;
    }

    /**
     * @return Document[]|null
     */
    public function getDocuments(): ?array
    {
        return $this->documents;
    }
}
