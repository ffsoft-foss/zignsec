<?php

namespace ffsoft\zignsec\messages\responses\scanning;

use ffsoft\zignsec\enums\AnalysisStatuses;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class AnalysisSummary
 *
 * @package ffsoft\zignsec\messages\responses\scanning
 */
class AnalysisSummary
{
    /**
     * @SerializedName("session_id")
     * @Type("string")
     * @see SessionResponse::$sessionId
     * @var string
     */
    protected $sessionId;
    /**
     * @SerializedName("session_status")
     * @Type("string")
     * @see AnalysisStatuses
     * @var string
     */
    protected $sessionStatus;
    /**
     * @SerializedName("session_parameters");
     * @Type("ffsoft\zignsec\messages\responses\scanning\SessionParameters")
     * @var SessionParameters|null
     */
    protected $sessionParameters;
    /**
     * @SerializedName("result")
     * @Type("string")
     * @see AnalysisStatuses
     * @var string
     */
    protected $result;
    /**
     * Result string representation.
     * @SerializedName("result_title")
     * @Type("string")
     *
     * @var string|null
     */
    protected $resultTitle;
    /**
     * Reason behind the result (usually – for DECLINED sessions).
     * @SerializedName("result_reason")
     * @Type("string")
     *
     * @var string|null
     */
    protected $resultReason;
    /**
     * @SerializedName("analysis_duration_sec")
     * @Type("string")
     * @var string|null
     */
    protected $analysisDurationSec;
    /**
     * @SerializedName("id_document_summary")
     * @Type("ffsoft\zignsec\messages\responses\scanning\IDDocumentSummary")
     * @var IDDocumentSummary|null
     */
    protected $idDocumentSummary;
    /**
     * @SerializedName("selfie_summary")
     * @Type("ffsoft\zignsec\messages\responses\scanning\SelfieSummary")
     * @var SelfieSummary|null
     */
    protected $selfieSummary;
    /**
     * @SerializedName("fraud_summary")
     * @Type("ffsoft\zignsec\messages\responses\scanning\FraudAnalysisSummary")
     * @var FraudAnalysisSummary|null
     */
    protected $fraudAnalysisSummary;
    /**
     * @SerializedName("expert_summary")
     * @Type("ffsoft\zignsec\messages\responses\scanning\ExpertAnalysisSummary")
     * @var ExpertAnalysisSummary|null
     */
    protected $expertAnalysisSummary;

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * @return string
     */
    public function getSessionStatus(): string
    {
        return $this->sessionStatus;
    }

    /**
     * @return SessionParameters|null
     */
    public function getSessionParameters(): ?SessionParameters
    {
        return $this->sessionParameters;
    }

    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * @return string|null
     */
    public function getResultTitle(): ?string
    {
        return $this->resultTitle;
    }

    /**
     * @return string|null
     */
    public function getResultReason(): ?string
    {
        return $this->resultReason;
    }

    /**
     * @return string|null
     */
    public function getAnalysisDurationSec(): ?string
    {
        return $this->analysisDurationSec;
    }

    /**
     * @return IDDocumentSummary|null
     */
    public function getIdDocumentSummary(): ?IDDocumentSummary
    {
        return $this->idDocumentSummary;
    }

    /**
     * @return SelfieSummary|null
     */
    public function getSelfieSummary(): ?SelfieSummary
    {
        return $this->selfieSummary;
    }

    /**
     * @return FraudAnalysisSummary|null
     */
    public function getFraudAnalysisSummary(): ?FraudAnalysisSummary
    {
        return $this->fraudAnalysisSummary;
    }

    /**
     * @return ExpertAnalysisSummary|null
     */
    public function getExpertAnalysisSummary(): ?ExpertAnalysisSummary
    {
        return $this->expertAnalysisSummary;
    }
}
