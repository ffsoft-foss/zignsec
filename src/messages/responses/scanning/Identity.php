<?php

namespace ffsoft\zignsec\messages\responses\scanning;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class Identity
 *
 * @package ffsoft\zignsec\messages\responses\scanning
 */
class Identity
{
    /**
     * Country code (ISO2).
     * @SerializedName("country_code")
     * @Type("string")
     *
     * @var string|null
     */
    protected $countryCode;
    /**
     * @SerializedName("first_name")
     * @Type("string")
     * @var string|null
     */
    protected $firstName;
    /**
     * @SerializedName("last_name")
     * @Type("string")
     * @var string|null
     */
    protected $lastName;
    /**
     * @SerializedName("full_name")
     * @Type("string")
     * @var string|null
     */
    protected $fullName;
    /**
     * @SerializedName("personal_number")
     * @Type("string")
     * @var string|null
     */
    protected $personalNumber;
    /**
     * @SerializedName("date_of_birth")
     * @Type("string")
     * @var string|null
     */
    protected $dateOfBirth;
    /**
     * @SerializedName("age")
     * @Type("integer")
     * @var int|null
     */
    protected $age;
    /**
     * Document expiration date if any.
     * @SerializedName("expiration")
     * @Type("string")
     *
     * @var string|null
     */
    protected $expiration;
    /**
     * True when document expired, false if expiration date is recognized and document is not expired,
     * null when expiration date is not recognized (or document analysis was skipped).
     * @SerializedName("expired")
     * @Type("integer")
     *
     * @var int|null
     */
    protected $expired;
    /**
     * The name of the state that issued a document.
     * @SerializedName("issuer_state")
     * @Type("string")
     *
     * @var string|null
     */
    protected $issuerState;
    /**
     * 'M' for male, 'F' – female, null – not recognized.
     * @SerializedName("gender")
     * @Type("string")
     *
     * @var string|null
     */
    protected $gender;
    /**
     * Id provider name (‘Scanning‘).
     * @SerializedName("id_provider_name")
     * @Type("string")
     *
     * @var string|null
     */
    protected $idProviderName;
    /**
     * Identification date (UTC).
     * @SerializedName("identification_date")
     * @Type("string")
     *
     * @var string|null
     */
    protected $identificationDate;
    /**
     * Internal id for the scanning provider.
     * @SerializedName("id_provider_request_id")
     * @Type("string")
     *
     * @var string|null
     */
    protected $idProviderRequestId;

    /**
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return string|null
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @return string|null
     */
    public function getPersonalNumber(): ?string
    {
        return $this->personalNumber;
    }

    /**
     * @return string|null
     */
    public function getDateOfBirth(): ?string
    {
        return $this->dateOfBirth;
    }

    /**
     * @return int|null
     */
    public function getAge(): ?int
    {
        return $this->age;
    }

    /**
     * @return string|null
     */
    public function getExpiration(): ?string
    {
        return $this->expiration;
    }

    /**
     * @return int|null
     */
    public function getExpired(): ?int
    {
        return $this->expired;
    }

    /**
     * @return string|null
     */
    public function getIssuerState(): ?string
    {
        return $this->issuerState;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @return string|null
     */
    public function getIdProviderName(): ?string
    {
        return $this->idProviderName;
    }

    /**
     * @return string|null
     */
    public function getIdentificationDate(): ?string
    {
        return $this->identificationDate;
    }

    /**
     * @return string|null
     */
    public function getIdProviderRequestId(): ?string
    {
        return $this->idProviderRequestId;
    }
}
