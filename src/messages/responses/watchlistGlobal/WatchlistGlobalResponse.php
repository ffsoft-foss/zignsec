<?php

namespace ffsoft\zignsec\messages\responses\watchlistGlobal;

use ffsoft\zignsec\messages\responses\BaseResponse;
use ffsoft\zignsec\messages\responses\watchlist\Persons;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class WatchlistGlobalResponse
 *
 * @package ffsoft\zignsec\messages\responses\watchlistGlobal
 */
class WatchlistGlobalResponse extends BaseResponse
{
    /**
     * @SerializedName("id")
     * @Type("string")
     *
     * @var string
     */
    public $id;
    /**
     * @SerializedName("errors")
     * @Type("array")
     *
     * @var array
     */
    public $errors;
    /**
     * @SerializedName("HitCountTotal")
     * @Type("integer")
     *
     * @var int
     */
    public $hit_count_total;
    /**
     * @SerializedName("HitCountPEP")
     * @Type("integer")
     *
     * @var int
     */
    public $hit_count_pep;
    /**
     * @SerializedName("HitCountSanctions")
     * @Type("integer")
     *
     * @var int
     */
    public $hit_count_sanctions;
    /**
     * @SerializedName("_DataSource")
     * @Type("string")
     *
     * @var string
     */
    public $data_source;
    /**
     * @SerializedName("Date")
     * @Type("string")
     *
     * @var string
     */
    public $date;
    /**
     * @SerializedName("Scan_ID")
     * @Type("string")
     *
     * @var string
     */
    public $scan_id;
    /**
     * @SerializedName("Number_Of_Matches")
     * @Type("integer")
     *
     * @var int
     */
    public $number_of_matches;
    /**
     * @SerializedName("Persons")
     * @Type("array<ffsoft\zignsec\messages\responses\watchlist\Persons>")
     *
     * @var Persons[]
     */
    public $persons;

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string[]
     */
    public function getErrors(): ?array
    {
        return $this->errors;
    }

    /**
     * @return int
     */
    public function getHitCountTotal(): ?int
    {
        return $this->hit_count_total;
    }

    /**
     * @return int
     */
    public function getHitCountPep(): ?int
    {
        return $this->hit_count_pep;
    }

    /**
     * @return int
     */
    public function getHitCountSanctions(): ?int
    {
        return $this->hit_count_sanctions;
    }

    /**
     * @return string
     */
    public function getDataSource(): ?string
    {
        return $this->data_source;
    }

    /**
     * @return string
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getScanId(): ?string
    {
        return $this->scan_id;
    }

    /**
     * @return int
     */
    public function getNumberOfMatches(): ?int
    {
        return $this->number_of_matches;
    }

    /**
     * @return Persons[]
     */
    public function getPersons(): ?array
    {
        return $this->persons;
    }
}
