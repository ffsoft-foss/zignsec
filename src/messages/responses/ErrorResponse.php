<?php

namespace ffsoft\zignsec\messages\responses;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class ErrorResponse
 *
 * @package ffsoft\zignsec\messages\responses
 */
class ErrorResponse extends BaseResponse
{
    /**
     * @SerializedName("errors")
     * @Type("array<ffsoft\zignsec\messages\responses\Error>")
     * @var Error[]
     */
    protected $errors;

    /**
     * @return Error[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
