<?php

namespace ffsoft\zignsec\messages\requests\getDocsAnalysisResults;

use ffsoft\zignsec\enums\Methods;
use ffsoft\zignsec\messages\requests\Request;
use ffsoft\zignsec\messages\responses\scanning\SessionResponse;
use ffsoft\zignsec\messages\responses\startDocsAnalysis\StartDocsAnalysisResponse;
use JMS\Serializer\Annotation\Exclude;

/**
 * Class GetDocsAnalysisResultsRequest
 *
 * @package ffsoft\zignsec\messages\requests\getDocsAnalysisResults
 */
class GetDocsAnalysisResultsRequest extends Request
{
    /**
     * @Exclude
     * @see SessionResponse::$sessionId
     * @var string
     */
    protected $scanningSessionId;
    /**
     * @Exclude
     * @see StartDocsAnalysisResponse::$analysisId
     * @var string
     */
    protected $analysisId;

    /**
     * GetDocsAnalysisResultsRequest constructor.
     *
     * @param string $scanningSessionId
     * @param string $analysisId
     */
    public function __construct(string $scanningSessionId, string $analysisId)
    {
        $this->scanningSessionId = $scanningSessionId;
        $this->analysisId = $analysisId;

        $this->method = Methods::GET_DOCS_ANALYSIS_RESULTS;
        $this->httpMethod = 'GET';
    }

    /**
     * @return string
     */
    public function getScanningSessionId(): string
    {
        return $this->scanningSessionId;
    }

    /**
     * @return string
     */
    public function getAnalysisId(): string
    {
        return $this->analysisId;
    }
}
