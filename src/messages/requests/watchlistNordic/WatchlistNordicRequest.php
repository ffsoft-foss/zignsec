<?php

namespace ffsoft\zignsec\messages\requests\watchlistNordic;

use ffsoft\zignsec\enums\Methods;
use ffsoft\zignsec\messages\requests\Address;
use ffsoft\zignsec\messages\requests\Request;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class WatchlistNordicRequest
 *
 * @package ffsoft\zignsec\messages\requests\watchlistNordic
 */
class WatchlistNordicRequest extends Request
{
    /**
     * [FirstName]
     * [LastName]
     * [DateOfBirth]
     * [CountryCode]
     * @SerializedName("Query")
     * @Type("ffsoft\zignsec\messages\requests\Address")
     *
     * @var Address
     */
    protected $query;

    /**
     * WatchlistNordicRequest constructor.
     *
     * @param Address $query
     */
    public function __construct(Address $query)
    {
        $this->query = $query;

        $this->method = Methods::WATCH_LIST_NORDIC;
        $this->httpMethod = 'POST';
    }

    /**
     * @return Address
     */
    public function getQuery(): Address
    {
        return $this->query;
    }
}
