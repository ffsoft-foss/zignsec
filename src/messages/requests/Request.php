<?php

namespace ffsoft\zignsec\messages\requests;

use JMS\Serializer\Annotation\Exclude;

/**
 * Class Request
 *
 * @package ffsoft\zignsec\messages\requests
 */
abstract class Request
{
    /**
     * @Exclude
     * @var string
     */
    protected $method;
    /**
     * @Exclude
     * @var string
     */
    protected $httpMethod;

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getHttpMethod(): string
    {
        return $this->httpMethod;
    }
}
