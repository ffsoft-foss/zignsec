<?php

namespace ffsoft\zignsec\messages\requests\watchlistGlobal;

use ffsoft\zignsec\enums\Methods;
use ffsoft\zignsec\messages\requests\Address;
use ffsoft\zignsec\messages\requests\Request;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class WatchlistGlobalRequest
 *
 * @package ffsoft\zignsec\messages\requests\watchlistGlobal
 */
class WatchlistGlobalRequest extends Request
{
    /**
     * [FirstName]
     * [LastName]
     * [DateOfBirth]
     * [CountryCode]
     * @SerializedName("Query")
     * @Type("ffsoft\zignsec\messages\requests\Address")
     *
     * @var Address
     */
    protected $query;
    /**
     * A percentage from 1-100, which indicates how closely the name being checked matches the name on the matching
     * watchlist profile, with 1% being not close and 100% being very close.
     * @SerializedName("MatchRate")
     * @Type("integer")
     *
     * @var int|null
     */
    protected $matchRate;
    /**
     * Can be pep or sanction. Leave as empty to scan on all lists.
     * @SerializedName("ListType")
     * @Type("string")
     *
     * @var string|null
     */
    protected $listType;
    /**
     * Only one of the two lists below can have a value and their value should be a comma-separated id. Eg: UN, SDN etc…
     *
     * @see WatchlistGlobalRequest::includedLists
     * @see WatchlistGlobalRequest::excludedLists
     * @see https://docs.zignsec.com/api-2/watchlist-pep-sanctions/#global-sanction-lists
     */
    /**
     * Use if you want to scan only over a specified sanction lists
     * @SerializedName("IncludedLists")
     * @Type("string")
     *
     * @var string|null
     */
    protected $includedLists;
    /**
     * If you wish to exclude particular sanction lists from your scan.
     * @SerializedName("ExcludedLists")
     * @Type("string")
     *
     * @var string|null
     */
    protected $excludedLists;

    /**
     * WatchlistGlobalRequest constructor.
     *
     * @param Address     $query
     * @param int|null    $matchRate
     * @param string|null $listType
     * @param string|null $includedLists
     * @param string|null $excludedLists
     */
    public function __construct(
        Address $query,
        ?int $matchRate = null,
        ?string $listType = null,
        ?string $includedLists = null,
        ?string $excludedLists = null
    ) {
        $this->query = $query;
        $this->matchRate = $matchRate;
        $this->listType = $listType;
        $this->includedLists = $includedLists;
        $this->excludedLists = $excludedLists;

        $this->method = Methods::WATCH_LIST_GLOBAL;
        $this->httpMethod = 'POST';
    }

    /**
     * @return Address
     */
    public function getQuery(): Address
    {
        return $this->query;
    }

    /**
     * @return int|null
     */
    public function getMatchRate(): ?int
    {
        return $this->matchRate;
    }

    /**
     * @return string|null
     */
    public function getListType(): ?string
    {
        return $this->listType;
    }

    /**
     * @return string|null
     */
    public function getIncludedLists(): ?string
    {
        return $this->includedLists;
    }

    /**
     * @return string|null
     */
    public function getExcludedLists(): ?string
    {
        return $this->excludedLists;
    }
}
