<?php

namespace ffsoft\zignsec\messages\requests\getDocsAnalysisStatus;

use ffsoft\zignsec\enums\Methods;
use ffsoft\zignsec\messages\requests\Request;
use ffsoft\zignsec\messages\responses\scanning\SessionResponse;
use JMS\Serializer\Annotation\Exclude;

/**
 * Class GetDocsAnalysisStatusRequest
 *
 * @package ffsoft\zignsec\messages\requests\getDocsAnalysisStatus
 */
class GetDocsAnalysisStatusRequest extends Request
{
    /**
     * @Exclude
     * @see SessionResponse::$sessionId
     * @var string
     */
    protected $scanningSessionId;

    /**
     * GetDocsAnalysisStatusRequest constructor.
     *
     * @param string $scanningSessionId
     */
    public function __construct(string $scanningSessionId)
    {
        $this->scanningSessionId = $scanningSessionId;

        $this->method = Methods::GET_DOCS_ANALYSIS_STATUS;
        $this->httpMethod = 'GET';
    }

    /**
     * @return string
     */
    public function getScanningSessionId(): string
    {
        return $this->scanningSessionId;
    }
}
