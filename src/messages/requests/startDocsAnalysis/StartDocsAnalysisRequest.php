<?php

namespace ffsoft\zignsec\messages\requests\startDocsAnalysis;

use ffsoft\zignsec\enums\AnalysisTypes;
use ffsoft\zignsec\enums\Methods;
use ffsoft\zignsec\messages\requests\Request;
use ffsoft\zignsec\messages\responses\scanning\SessionResponse;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

class StartDocsAnalysisRequest extends Request
{
    /**
     * @Exclude
     * @see SessionResponse::$sessionId
     * @var string
     */
    protected $scanningSessionId;
    /**
     * @SerializedName("analysis_types")
     * @Type("array")
     * @see AnalysisTypes
     * @var array
     */
    protected $analysisTypes;
    /**
     * Client time zone offset in minutes.
     *
     * @Exclude
     * @var int|null
     */
    protected $timeZoneOffset;

    /**
     * StartDocsAnalysisRequest constructor.
     *
     * @param string   $scanningSessionId
     * @param array    $analysisTypes
     * @param int|null $timeZoneOffset
     */
    public function __construct(string $scanningSessionId, array $analysisTypes, ?int $timeZoneOffset = null)
    {
        $this->scanningSessionId = $scanningSessionId;
        $this->analysisTypes = $analysisTypes;
        $this->timeZoneOffset = $timeZoneOffset;

        $this->method = Methods::START_DOCS_ANALYSIS;
        $this->httpMethod = 'POST';
    }

    /**
     * @return string
     */
    public function getScanningSessionId(): string
    {
        return $this->scanningSessionId;
    }

    /**
     * @return array
     */
    public function getAnalysisTypes(): array
    {
        return $this->analysisTypes;
    }

    /**
     * @return int|null
     */
    public function getTimeZoneOffset(): ?int
    {
        return $this->timeZoneOffset;
    }
}
