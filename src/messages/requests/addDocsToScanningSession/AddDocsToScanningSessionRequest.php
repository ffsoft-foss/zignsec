<?php

namespace ffsoft\zignsec\messages\requests\addDocsToScanningSession;

use CURLFile;
use ffsoft\zignsec\enums\DocumentTypes;
use ffsoft\zignsec\enums\Methods;
use ffsoft\zignsec\messages\requests\Request;
use ffsoft\zignsec\messages\responses\scanning\SessionResponse;
use JMS\Serializer\Annotation\Exclude;

/**
 * Class AddDocsToScanningSessionRequest
 *
 * @package ffsoft\zignsec\messages\requests\addDocsToScanningSession
 */
class AddDocsToScanningSessionRequest extends Request
{
    /**
     * @Exclude
     * @see SessionResponse::$sessionId
     * @var string
     */
    protected $scanningSessionId;
    /**
     * @Exclude
     * @see DocumentTypes
     * @var string
     */
    protected $documentType;
    /**
     * Files to be analysed
     *
     * @Exclude
     * @see https://wiki.php.net/rfc/curl-file-upload
     * @var CURLFile[]
     */
    protected $curlFiles;
    /**
     * The document reference.
     *
     * @Exclude
     * @var string|null
     */
    protected $reference;

    /**
     * AddDocsToScanningSessionRequest constructor.
     *
     * @param string      $scanningSessionId
     * @param string      $documentType
     * @param CURLFile[]  $curlFiles
     * @param string|null $reference
     */
    public function __construct(
        string $scanningSessionId,
        string $documentType,
        array $curlFiles,
        ?string $reference = null
    ) {
        $this->scanningSessionId = $scanningSessionId;
        $this->documentType = $documentType;
        $this->curlFiles = $curlFiles;
        $this->reference = $reference;

        $this->method = Methods::ADD_DOCS_TO_SCANNING_SESSION;
        $this->httpMethod = 'POST';
    }

    /**
     * @return string
     */
    public function getScanningSessionId(): string
    {
        return $this->scanningSessionId;
    }

    /**
     * @return string
     */
    public function getDocumentType(): string
    {
        return $this->documentType;
    }

    /**
     * @return CURLFile[]
     */
    public function getCurlFiles(): array
    {
        return $this->curlFiles;
    }

    /**
     * @return string|null
     */
    public function getReference(): ?string
    {
        return $this->reference;
    }
}
