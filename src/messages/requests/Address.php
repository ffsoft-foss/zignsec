<?php

namespace ffsoft\zignsec\messages\requests;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class Address
 *
 * @package ffsoft\zignsec\messages\requests
 */
class Address
{
    /**
     * Normally blank, but can be set to ex Protected, Deceased, Emmigrated, De-registered,
     * where the rest of the fields will be blank.
     * @SerializedName("PersonStatus")
     * @Type("string")
     *
     * @var string|null
     */
    protected $personStatus;
    /**
     * @SerializedName("FirstName")
     * @Type("string")
     * @var string|null
     */
    protected $firstName;
    /**
     * @SerializedName("LastName")
     * @Type("string")
     * @var string|null
     */
    protected $lastName;
    /**
     * A string in the format YYYY-MM-DD.
     * @SerializedName("DateOfBirth")
     * @Type("string")
     *
     * @var string|null
     */
    protected $dateOfBirth;
    /**
     * Sets the gender of searched person. Female or Male.
     * @SerializedName("Gender")
     * @Type("string")
     *
     * @var string|null
     */
    protected $gender;
    /**
     * Only if delivered from an underlying search provider.
     * @SerializedName("PersonalNumber")
     * @Type("string")
     *
     * @var string|null
     */
    protected $personalNumber;
    /**
     * Most often the street name and number.
     * @SerializedName("Address")
     * @Type("string")
     *
     * @var string|null
     */
    protected $address;
    /**
     * Most often empty. An extra address line to be used for example for c/o addresses.
     * @SerializedName("Address2")
     * @Type("string")
     *
     * @var string|null
     */
    protected $address2;
    /**
     * This is the zip code. Most often a numeric string.
     * @SerializedName("PostalCode")
     * @Type("string")
     *
     * @var string|null
     */
    protected $postalCode;
    /**
     * Most often empty. This is an extra city locality field. Probably used in the United Kingdom.
     * @SerializedName("Location")
     * @Type("string")
     *
     * @var string|null
     */
    protected $location;
    /**
     * Is often paired with the PostalCode.
     * @SerializedName("City")
     * @Type("string")
     *
     * @var string|null
     */
    protected $city;
    /**
     * The two-letter iso country code. Identifies the country of the address.
     * @SerializedName("CountryCode")
     * @Type("string")
     *
     * @var string|null
     */
    protected $countryCode;
    /**
     * Meta field.
     * For traceability this field shows the name of the external data service, where the information originated from.
     * @SerializedName("_DataSource")
     * @Type("string")
     *
     * @var string|null
     */
    protected $dataSource;
    /**
     * Meta field. On search results.
     * Descriptive string that is set when the address are returned from a SearchPerson call.
     * Sections are delimited with |. This is a the degree of matching to the search query.
     * Can be either HIGH, MEDIUM or LOW together with a percentage score.
     * All rating score limits can be changed after specific customer requirements. Followed by subscore description.
     * @SerializedName("_MatchLevel")
     * @Type("string")
     *
     * @var string|null
     */
    protected $matchLevel;
    /**
     * Meta field. Only for Search Results.
     * Most often the search results at the underlying data provider are sorted in order of relevance.
     * This is a field to keep that information.
     * @SerializedName("_SortIndexAtSource")
     * @Type("string")
     *
     * @var string|null
     */
    protected $sortIndexAtSource;

    /**
     * @return string|null
     */
    public function getPersonStatus(): ?string
    {
        return $this->personStatus;
    }

    /**
     * @param string|null $personStatus
     *
     * @return Address
     */
    public function setPersonStatus(?string $personStatus): Address
    {
        $this->personStatus = $personStatus;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     *
     * @return Address
     */
    public function setFirstName(?string $firstName): Address
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     *
     * @return Address
     */
    public function setLastName(?string $lastName): Address
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDateOfBirth(): ?string
    {
        return $this->dateOfBirth;
    }

    /**
     * @param string|null $dateOfBirth
     *
     * @return Address
     */
    public function setDateOfBirth(?string $dateOfBirth): Address
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param string|null $gender
     *
     * @return Address
     */
    public function setGender(?string $gender): Address
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPersonalNumber(): ?string
    {
        return $this->personalNumber;
    }

    /**
     * @param string|null $personalNumber
     *
     * @return Address
     */
    public function setPersonalNumber(?string $personalNumber): Address
    {
        $this->personalNumber = $personalNumber;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     *
     * @return Address
     */
    public function setAddress(?string $address): Address
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    /**
     * @param string|null $address2
     *
     * @return Address
     */
    public function setAddress2(?string $address2): Address
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    /**
     * @param string|null $postalCode
     *
     * @return Address
     */
    public function setPostalCode(?string $postalCode): Address
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @param string|null $location
     *
     * @return Address
     */
    public function setLocation(?string $location): Address
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     *
     * @return Address
     */
    public function setCity(?string $city): Address
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    /**
     * @param string|null $countryCode
     *
     * @return Address
     */
    public function setCountryCode(?string $countryCode): Address
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDataSource(): ?string
    {
        return $this->dataSource;
    }

    /**
     * @param string|null $dataSource
     *
     * @return Address
     */
    public function setDataSource(?string $dataSource): Address
    {
        $this->dataSource = $dataSource;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMatchLevel(): ?string
    {
        return $this->matchLevel;
    }

    /**
     * @param string|null $matchLevel
     *
     * @return Address
     */
    public function setMatchLevel(?string $matchLevel): Address
    {
        $this->matchLevel = $matchLevel;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSortIndexAtSource(): ?string
    {
        return $this->sortIndexAtSource;
    }

    /**
     * @param string|null $sortIndexAtSource
     *
     * @return Address
     */
    public function setSortIndexAtSource(?string $sortIndexAtSource): Address
    {
        $this->sortIndexAtSource = $sortIndexAtSource;

        return $this;
    }
}
