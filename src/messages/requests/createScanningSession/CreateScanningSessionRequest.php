<?php

namespace ffsoft\zignsec\messages\requests\createScanningSession;

use ffsoft\zignsec\enums\AnalysisTypes;
use ffsoft\zignsec\enums\Methods;
use ffsoft\zignsec\messages\requests\Request;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Class CreateScanningSessionRequest
 *
 * @package ffsoft\zignsec\messages\requests\createScanningSession
 */
class CreateScanningSessionRequest extends Request
{
    /**
     * This semi-optional parameter will be returned to you at the redirect back to your server. Use it to link
     * an unique ID of your choice that you can parse. If not relaystate is specified, ZignSec will automatically set
     * the ZignSec’s unique session identifier, the RequestID token here.
     * @SerializedName("relay_state")
     * @Type("string")
     *
     * @var string|null
     */
    protected $relayState;
    /**
     * Array of analysis to perform.
     * @SerializedName("analysis_types")
     * @Type("array")
     *
     * @see AnalysisTypes
     *
     * @var array|null
     */
    protected $analysisTypes;
    /**
     * This parameter is supplied the browser session will finally be redirected to this URL-value.
     * @SerializedName("target")
     * @Type("string")
     *
     * @var string|null
     */
    protected $target;
    /**
     * Works as "target" except it is navigated on user cancel or error situations.
     * @SerializedName("target_error")
     * @Type("string")
     *
     * @var string|null
     */
    protected $targetError;
    /**
     * A URL where success/error results will automatically be POSTed.
     * @SerializedName("webhook")
     * @Type("string")
     *
     * @var string|null
     */
    protected $webhook;

    /**
     * CreateScanningSessionRequest constructor.
     *
     * @param string|null $relayState
     * @param array|null  $analysisTypes
     * @param string|null $target
     * @param string|null $targetError
     * @param string|null $webhook
     */
    public function __construct(
        ?string $relayState = null,
        ?array $analysisTypes = null,
        ?string $target = null,
        ?string $targetError = null,
        ?string $webhook = null
    ) {
        $this->relayState = $relayState;
        $this->analysisTypes = $analysisTypes;
        $this->target = $target;
        $this->targetError = $targetError;
        $this->webhook = $webhook;

        $this->method = Methods::CREATE_SCANNING_SESSION;
        $this->httpMethod = 'POST';
    }
}
