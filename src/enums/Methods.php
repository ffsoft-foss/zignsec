<?php

namespace ffsoft\zignsec\enums;

/**
 * Class Methods
 *
 * @package ffsoft\zignsec\enums
 */
class Methods
{
    public const WATCH_LIST_NORDIC = 1;
    public const WATCH_LIST_GLOBAL = 2;
    public const CREATE_SCANNING_SESSION = 3;
    public const ADD_DOCS_TO_SCANNING_SESSION = 4;
    public const START_DOCS_ANALYSIS = 5;
    public const GET_DOCS_ANALYSIS_STATUS = 6;
    public const GET_DOCS_ANALYSIS_RESULTS = 7;
    public static $endpoints
        = [
            self::WATCH_LIST_NORDIC            => '/v2/ekyc/watchlist/nordic',
            self::WATCH_LIST_GLOBAL            => '/v2/ekyc/watchlist/global',
            self::CREATE_SCANNING_SESSION      => '/v3/eid/scanningsessions',
            self::ADD_DOCS_TO_SCANNING_SESSION => '/v3/eid/scanningsessions/{%scanningSessionId%}/documents',
            self::START_DOCS_ANALYSIS          => '/v3/eid/scanningsessions/{%scanningSessionId%}/analyses/',
            self::GET_DOCS_ANALYSIS_STATUS     => '/v3/eid/scanningsessions/{%scanningSessionId%}/analysisStatus',
            self::GET_DOCS_ANALYSIS_RESULTS    => '/v3/eid/scanningsessions/{%scanningSessionId%}/analyses/{%analysisId%}',
        ];

    /**
     * Get endpoint by enum
     *
     * @return string|null $endpoint
     * @var int $enum
     *
     */
    public static function getEndpoint(int $enum): ?string
    {
        return static::$endpoints[$enum] ?? null;
    }
}
