<?php

namespace ffsoft\zignsec\enums;

/**
 * Class AnalysisStates
 *
 * @package ffsoft\zignsec\enums
 */
class AnalysisStates
{
    public const NOT_REQUESTED = 'NotRequested';
    public const PROCESSING = 'Processing';
    public const FINISHED = 'Finished';
    public const FAILED = 'Failed';
    public static $description
        = [
            self::NOT_REQUESTED => 'Analysis not requested',
            self::PROCESSING    => 'Analysis is in progress',
            self::FINISHED      => 'Analysis finished, check result',
            self::FAILED        => 'An internal error occurred during analysis',
        ];
}
