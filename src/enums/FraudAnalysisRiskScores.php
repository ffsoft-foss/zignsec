<?php

namespace ffsoft\zignsec\enums;

/**
 * Class FraudAnalysisRiskScores
 *
 * @package ffsoft\zignsec\enums
 */
class FraudAnalysisRiskScores
{
    public const UNDEFINED = 'Undefined';
    public const NO_RISK = 'NoRisk';
    public const LOW_RISK = 'LowRisk';
    public const MEDIUM_RISK = 'MediumRisk';
    public const HIGH_RISK = 'HighRisk';
    public static $description
        = [
            self::UNDEFINED   => 'Risk is not calculated',
            self::NO_RISK     => 'No risk',
            self::LOW_RISK    => 'Low risk',
            self::MEDIUM_RISK => 'Medium risk',
            self::HIGH_RISK   => 'High risk',
        ];
}
