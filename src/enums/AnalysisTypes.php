<?php

namespace ffsoft\zignsec\enums;

/**
 * Class AnalysisTypes
 *
 * @package ffsoft\zignsec\enums
 */
class AnalysisTypes
{
    public const DOCUMENT = 'document';
    public const FRAUD = 'fraud';
    public const SELFIE = 'selfie';
    public const EXPERT = 'expert';
    public static $description
        = [
            self::DOCUMENT => 'Analyses and verifies the content of the uploaded image of the id-card, passport or driver’s license',
            self::FRAUD    => 'Analyses the uploaded document/image to check if it has been manipulated with image processing software',
            self::SELFIE   => 'Automatically finds analyses and evaluates the bio-metric resemblance of faces represented in uploaded images',
            self::EXPERT   => 'This analysis type should be considered additional and it is recommended as an separate request to be run by a compliance officer to get more information about the uploaded document/image.',
        ];
}
