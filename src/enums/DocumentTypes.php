<?php

namespace ffsoft\zignsec\enums;

/**
 * Class DocumentTypes
 *
 * @package ffsoft\zignsec\enums
 */
class DocumentTypes
{
    public const PASSPORT = 'Passport';
    public const IDENTITY_CARD = 'IdentityCard';
    public const DRIVERS_LICENSE = 'DriversLicense';
    public const SELFIE = 'Selfie';
    public const OTHER_DOCUMENT = 'OtherDocument';
    public static $description
        = [
            self::PASSPORT        => 'Passport – the page with face photo required',
            self::IDENTITY_CARD   => 'ID card – 2 sides required',
            self::DRIVERS_LICENSE => 'Driver license – the side with photo required',
            self::SELFIE          => 'Selfie (a biometric image)',
            self::OTHER_DOCUMENT  => 'Other document – the page without photo required',
        ];
}
