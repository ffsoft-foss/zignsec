<?php

namespace ffsoft\zignsec\enums;

/**
 * Class AnalysisStatuses
 *
 * @package ffsoft\zignsec\enums
 */
class AnalysisStatuses
{
    public const NOT_REQUESTED = 'NOT_REQUESTED';
    public const ANALYSING = 'ANALYSING';
    public const FAILED = 'FAILED';
    public const ACCEPTED = 'ACCEPTED';
    public const OPERATOR_REQUIRED = 'OPERATOR_REQUIRED';
    public const DECLINED = 'DECLINED';
    public const TIMEOUT = 'TIMEOUT';
    public static $description
        = [
            self::NOT_REQUESTED     => 'Analysis not requested',
            self::ANALYSING         => 'Analysis is in progress',
            self::FAILED            => 'Internal error occurred during analysis',
            self::ACCEPTED          => 'Analysis finished, documents have been ACCEPTED',
            self::OPERATOR_REQUIRED => 'Analysis finished, OPERATOR REQUIRED to make a final decision',
            self::DECLINED          => 'Analysis finished, documents have been DECLINED',
            self::TIMEOUT           => 'TIme out waiting for the response from the scanning provider. there is still a chance that session could be completed manually (from the admin UI)',
        ];
}
