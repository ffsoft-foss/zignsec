<?php

namespace ffsoft\zignsec\exceptions;

use Exception;

/**
 * Class ClientException means Client layer's exception
 *
 * @package ffsoft\zignsec\exceptions
 */
class ClientException extends Exception
{
}
