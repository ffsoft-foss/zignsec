FROM debian:buster

## Environment settings
ENV DEBIAN_FRONTEND=noninteractive
ENV COMPOSER_ALLOW_SUPERUSER=1

## Install compile dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        git \
        vim \
        curl \
        less \
        htop \
        lsof \
        net-tools \
        iputils-ping \
        unzip \
        ca-certificates \
        php7.3 \
        php7.3-soap \
        php7.3-zip \
        php7.3-curl \
        php7.3-bcmath \
        php7.3-exif \
        php7.3-gd \
        php7.3-iconv \
        php7.3-intl \
        php7.3-mbstring \
        php7.3-simplexml \
        php7.3-dom \
        php7.3-opcache \
        php7.3-mysql \
        php7.3-memcached \
        php7.3-dev \
        php-pear \
        build-essential

## Clean up
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

## Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --filename=composer --install-dir=/usr/local/bin && \
    composer clear-cache

## Set working directory
WORKDIR /opt/app